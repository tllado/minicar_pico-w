#include "../common/ws2812.h"

////////////////////////////////////////////////////////////////////////////////
// Behavior Config
////////////////////////////////////////////////////////////////////////////////

#define BUTTON_LONG_PRESS 500U  // ms
#define CUTOFF_VELOCITY   0.1F
#define CUTOFF_STEERING   0.1F
#define FILTER_LENGTH     5U    // samples
#define TICK_PERIOD       20U   // ms

const uint32_t joystick_neutral_color{brightness(COLOR_WHITE, 0.02F)};

const uint32_t drive_direction_colors[2U] {
  brightness(COLOR_GREEN, 0.075F),  // this_way
  brightness(COLOR_RED, 0.125F)     // that_way
};

const uint32_t steering_mode_colors[3U] {
  brightness(COLOR_GREEN, 0.075F),  // four_wheel_steer
  brightness(COLOR_RED, 0.125F),    // two_wheel_steer
  brightness(COLOR_BLUE, 0.125F)    // four_wheel_translate
};

////////////////////////////////////////////////////////////////////////////////
// Hardware Config
////////////////////////////////////////////////////////////////////////////////

#define PIN_BUTTON      26U
#define PIN_JOYSTICK_X  28U
#define PIN_JOYSTICK_Y  27U
#define PIN_LEDS        8U

#define ADC_JOYSTICK_X  2U
#define ADC_JOYSTICK_Y  1U
#define ADC_RESOLUTION  4096U

// Scale signals up so we can have max velocity and max turning simultaneously
#define JOYSTICK_CIRCLE_SCALE (1.414F)  // sqrt(2), 1/cos(45deg)

// Set bounds of each joystick direction as determined by multimeter
#define JOYSTICK_FWD_SCALE  (1.00F/0.70F)
#define JOYSTICK_REV_SCALE  (1.00F/0.60F)
#define JOYSTICK_RGT_SCALE  (1.00F/0.61F)
#define JOYSTICK_LFT_SCALE  (1.00F/0.58F)

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
