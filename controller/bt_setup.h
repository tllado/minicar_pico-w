#define HEARTBEAT_PERIOD_MS 20
#define LED_SLOW_FLASH_DELAY_MS 1000

int bt_sender_init(void(*task)(void), uint32_t* message);
int bt_sender_start();

int bt_receiver_init(void(*task)(void), uint32_t* message);
void bt_receiver_start();