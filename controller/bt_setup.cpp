////////////////////////////////////////////////////////////////////////////////

#include "btstack.h"
#include "pico/cyw43_arch.h"
#include "pico/btstack_cyw43.h"
#include "temp_sensor.h"

#include "bt_setup.h"

////////////////////////////////////////////////////////////////////////////////

#define APP_AD_FLAGS 0x06

////////////////////////////////////////////////////////////////////////////////

btstack_timer_source_t global_heartbeat;
btstack_packet_callback_registration_t global_hci_event_callback_registration;

int global_le_notification_enabled;
hci_con_handle_t global_con_handle;

void(*global_callback_task)(void);
uint32_t* global_callback_message;

uint8_t global_adv_data[] = {
    // Flags general discoverable
    0x02, BLUETOOTH_DATA_TYPE_FLAGS, APP_AD_FLAGS,
    // Name
    0x17, BLUETOOTH_DATA_TYPE_COMPLETE_LOCAL_NAME, 'P', 'i', 'c', 'o', ' ', '0', '0', ':', '0', '0', ':', '0', '0', ':', '0', '0', ':', '0', '0', ':', '0', '0',
    0x03, BLUETOOTH_DATA_TYPE_COMPLETE_LIST_OF_16_BIT_SERVICE_CLASS_UUIDS, 0x1a, 0x18,
};
const uint8_t global_adv_data_len = sizeof(global_adv_data);

////////////////////////////////////////////////////////////////////////////////

uint16_t att_read_callback(hci_con_handle_t connection_handle, uint16_t att_handle, uint16_t offset, uint8_t * buffer, uint16_t buffer_size);
int att_write_callback(hci_con_handle_t connection_handle, uint16_t att_handle, uint16_t transaction_mode, uint16_t offset, uint8_t *buffer, uint16_t buffer_size);
void heartbeat_handler(struct btstack_timer_source *ts);
void packet_handler(uint8_t packet_type, uint16_t channel, uint8_t *packet, uint16_t size);

////////////////////////////////////////////////////////////////////////////////

uint16_t att_read_callback(hci_con_handle_t connection_handle, uint16_t att_handle, uint16_t offset, uint8_t * buffer, uint16_t buffer_size) {
    UNUSED(connection_handle);

    if (att_handle == ATT_CHARACTERISTIC_ORG_BLUETOOTH_CHARACTERISTIC_TEMPERATURE_01_VALUE_HANDLE){
        return att_read_callback_handle_blob((const uint8_t *)&global_callback_message, sizeof(global_callback_message), offset, buffer, buffer_size);
    }
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

int att_write_callback(hci_con_handle_t connection_handle, uint16_t att_handle, uint16_t transaction_mode, uint16_t offset, uint8_t *buffer, uint16_t buffer_size) {
    UNUSED(transaction_mode);
    UNUSED(offset);
    UNUSED(buffer_size);
    
    if (att_handle != ATT_CHARACTERISTIC_ORG_BLUETOOTH_CHARACTERISTIC_TEMPERATURE_01_CLIENT_CONFIGURATION_HANDLE) return 0;
    global_le_notification_enabled = little_endian_read_16(buffer, 0) == GATT_CLIENT_CHARACTERISTICS_CONFIGURATION_NOTIFICATION;
    global_con_handle = connection_handle;
    if (global_le_notification_enabled) {
        att_server_request_can_send_now_event(global_con_handle);
    }
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

int bt_sender_init(void(*task)(void), uint32_t* message) {
    global_callback_task = task;
    global_callback_message = message;

    // initialize CYW43 driver architecture (will enable BT if/because CYW43_ENABLE_BLUETOOTH == 1)
    if (cyw43_arch_init()) {
        printf("failed to initialise cyw43_arch\n");
        return -1;
    }

    // Initialize rest of BT Stack
    l2cap_init();
    sm_init();
    att_server_init(profile_data, att_read_callback, att_write_callback);

    // inform about BTstack state
    global_hci_event_callback_registration.callback = &packet_handler;
    hci_add_event_handler(&global_hci_event_callback_registration);

    // register for ATT event
    att_server_register_packet_handler(packet_handler);

    // set one-shot btstack timer
    global_heartbeat.process = &heartbeat_handler;
    btstack_run_loop_set_timer(&global_heartbeat, HEARTBEAT_PERIOD_MS);
    btstack_run_loop_add_timer(&global_heartbeat);

    return 0;
}

////////////////////////////////////////////////////////////////////////////////

int bt_sender_start() {
    // turn on bluetooth!
    hci_power_control(HCI_POWER_ON);

    return 0;
}

////////////////////////////////////////////////////////////////////////////////

void heartbeat_handler(struct btstack_timer_source *ts) {
    static uint32_t counter = 0;
    counter++;

    global_callback_task();
    if (global_le_notification_enabled) {
        att_server_request_can_send_now_event(global_con_handle);
    }

    // Restart timer
    btstack_run_loop_set_timer(ts, HEARTBEAT_PERIOD_MS);
    btstack_run_loop_add_timer(ts);
}

////////////////////////////////////////////////////////////////////////////////

void packet_handler(uint8_t packet_type, uint16_t channel, uint8_t *packet, uint16_t size) {
    UNUSED(size);
    UNUSED(channel);
    bd_addr_t local_addr;
    if (packet_type != HCI_EVENT_PACKET) return;

    uint8_t event_type = hci_event_packet_get_type(packet);
    switch(event_type){
        case BTSTACK_EVENT_STATE:{
            if (btstack_event_state_get_state(packet) != HCI_STATE_WORKING) return;
            gap_local_bd_addr(local_addr);
            printf("BTstack up and running on %s.\n", bd_addr_to_str(local_addr));

            // setup advertisements
            uint16_t adv_int_min = 800;
            uint16_t adv_int_max = 800;
            uint8_t adv_type = 0;
            bd_addr_t null_addr;
            memset(null_addr, 0, 6);
            gap_advertisements_set_params(adv_int_min, adv_int_max, adv_type, 0, null_addr, 0x07, 0x00);
            assert(global_adv_data_len <= 31); // ble limitation
            gap_advertisements_set_data(global_adv_data_len, (uint8_t*) global_adv_data);
            gap_advertisements_enable(1);

            global_callback_task();

            break;}
        case HCI_EVENT_DISCONNECTION_COMPLETE:
            global_le_notification_enabled = 0;
            break;
        case ATT_EVENT_CAN_SEND_NOW:
            att_server_notify(global_con_handle, ATT_CHARACTERISTIC_ORG_BLUETOOTH_CHARACTERISTIC_TEMPERATURE_01_VALUE_HANDLE, (uint8_t*)global_callback_message, sizeof(global_callback_message));
            break;
        default:
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////
