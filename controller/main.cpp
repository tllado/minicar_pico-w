////////////////////////////////////////////////////////////////////////////////
// MiniZoox Controller, Pico-W v0.9.1
// written by Travis Llado
// last edited 2023-11-20
////////////////////////////////////////////////////////////////////////////////

#define DEBUG 0

////////////////////////////////////////////////////////////////////////////////

#if DEBUG
  #include <stdio.h>
#endif

#include "pico/stdlib.h"
#include "hardware/adc.h"

#include "../common/car_types.h"
#include "../common/llado_math.h"
#include "../common/ws2812.h"

#include "bt_setup.h"
#include "controller_config.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables
////////////////////////////////////////////////////////////////////////////////

uint32_t message{};
VehicleCommand command{};

mean_filter steering_filter(FILTER_LENGTH);
mean_filter velocity_filter(FILTER_LENGTH);

float steering_offset{0.0F};
float velocity_offset{0.0F};

////////////////////////////////////////////////////////////////////////////////

void init_joystick(void) {
  adc_init();
  adc_gpio_init(PIN_JOYSTICK_X);
  adc_gpio_init(PIN_JOYSTICK_Y);

  for (uint32_t i = 0; i < FILTER_LENGTH; i++) {
    sleep_ms(TICK_PERIOD);

    adc_select_input(ADC_JOYSTICK_X);
    steering_offset = steering_filter.update(adc_read());

    adc_select_input(ADC_JOYSTICK_Y);
    velocity_offset = velocity_filter.update(adc_read());
  }
}

void update_steering_command(void) {
  adc_select_input(ADC_JOYSTICK_X);
  const float filtered_steering =
      -1.0F * (steering_filter.update(adc_read()) - steering_offset) / 
      (ADC_RESOLUTION / 2.0F);

  const float scaled_steering = (filtered_steering >= 0.0F) ? 
      (filtered_steering * JOYSTICK_RGT_SCALE * JOYSTICK_CIRCLE_SCALE) : 
      (filtered_steering * JOYSTICK_LFT_SCALE * JOYSTICK_CIRCLE_SCALE);

  if (abs(scaled_steering) > 0.99F) {
    command.steering = 0.99F * sign(scaled_steering);
  } else if (abs(scaled_steering) < CUTOFF_STEERING) {
    command.steering = 0.0F;
  } else {
    command.steering = 
        (abs(scaled_steering) - CUTOFF_STEERING) * sign(scaled_steering) * 
        1.0F / (1.0F - CUTOFF_STEERING);
  }
}

void update_velocity_command(void) {
  adc_select_input(ADC_JOYSTICK_Y);
  const float filtered_velocity =
      -1.0F * (velocity_filter.update(adc_read() - velocity_offset)) / 
      (ADC_RESOLUTION / 2.0F);

  const float scaled_velocity = (filtered_velocity >= 0.0F) ? 
      (filtered_velocity * JOYSTICK_FWD_SCALE * JOYSTICK_CIRCLE_SCALE) : 
      (filtered_velocity * JOYSTICK_REV_SCALE * JOYSTICK_CIRCLE_SCALE);

  if (abs(scaled_velocity) > 0.99F) {
    command.velocity = 0.99F * sign(scaled_velocity);
  } else if (abs(scaled_velocity) < CUTOFF_VELOCITY) {
    command.velocity = 0.0F;
  } else {
    command.velocity = 
        (abs(scaled_velocity) - CUTOFF_VELOCITY) * sign(scaled_velocity) * 
        1.0F / (1.0F - CUTOFF_VELOCITY);
  }
}

////////////////////////////////////////////////////////////////////////////////

void init_button(void) {
  gpio_init(PIN_BUTTON);
  gpio_set_dir(PIN_BUTTON, GPIO_IN);
  gpio_pull_up(PIN_BUTTON);
}

void update_button(void) {
  static uint32_t press_length{0U};

  const bool button_pressed = !gpio_get(PIN_BUTTON);

  if (button_pressed) {
    if (press_length == (BUTTON_LONG_PRESS / TICK_PERIOD)) {
      // long press completed
      command.steering_mode = 
          static_cast<SteeringMode>(
            (static_cast<uint8_t>(command.steering_mode) + 1U) % 
            static_cast<uint8_t>(SteeringMode::size));
    }
    press_length++;
  } else {
    if ((press_length > 0U) && 
        (press_length < (BUTTON_LONG_PRESS / TICK_PERIOD))) {
      // short press completed
      command.drive_direction = 
          static_cast<DriveDirection>(
            (static_cast<uint8_t>(command.drive_direction) + 1U) % 
            static_cast<uint8_t>(DriveDirection::size));
    }
    press_length = 0U;
  }
}

////////////////////////////////////////////////////////////////////////////////

void update_lights(VehicleCommand command) {
  const uint32_t color_velocity_command = 
      blend(joystick_neutral_color, 
          ((command.velocity > CUTOFF_VELOCITY) ? COLOR_GREEN : COLOR_RED), 
          (command.velocity * command.velocity));

  const uint32_t color_steering_command = 
      blend(joystick_neutral_color,
          ((command.steering > CUTOFF_VELOCITY) ? COLOR_GREEN : COLOR_RED), 
          (command.steering * command.steering));

  const std::vector<uint32_t> led_values {
    color_velocity_command,
    color_steering_command,
    drive_direction_colors[static_cast<uint8_t>(command.drive_direction)],
    steering_mode_colors[static_cast<uint8_t>(command.steering_mode)]
  };

  update_ws2812(led_values);
}

////////////////////////////////////////////////////////////////////////////////

void bt_callback(void) {

  update_button();
  update_velocity_command();
  update_steering_command();
  update_lights(command);

  message = 
      ((uint16_t((command.velocity + 1.0F) * 2048.0F) & 0x0FFF) << 20U) +
      ((uint16_t((command.steering + 1.0F) * 2048.0F) & 0x0FFF) << 8U) +
      (static_cast<uint8_t>(command.drive_direction) << 2U) +
      (static_cast<uint8_t>(command.steering_mode));

  #if DEBUG
    printf("sent %f %f %u %u\n", 
        command.velocity,
        command.steering,
        static_cast<uint16_t>(command.drive_direction),
        static_cast<uint16_t>(command.steering_mode));
  #endif
 }

////////////////////////////////////////////////////////////////////////////////

int main() {
  #if DEBUG
    stdio_init_all();
  #endif

  init_joystick();
  init_ws2812(PIN_LEDS);
  init_button();
  bt_sender_init(&bt_callback, &message);
  bt_sender_start();
    
  while(true) {   
    // Do nothing, all further activity handled by bt handlers   
    sleep_ms(1000);
  }
    
  return 0;
}

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
