////////////////////////////////////////////////////////////////////////////////
// Behavior Config
////////////////////////////////////////////////////////////////////////////////

#define CONTROL_LOOP_FREQ   50  // Hz

#define CUTOFF_BRAKING      0.1F
#define CUTOFF_STEERING     0.001F
#define CUTOFF_TURN_SIGNAL  0.2F
#define CUTOFF_VELOCITY     0.001F
#define BLINK_FREQUENCY     3.5F  // hz
#define DRIVE_JERK_LIMIT    5.0F  // *100%_single-ended_range/sec

#define BLINK_TICKS uint32_t(CONTROL_LOOP_FREQ/BLINK_FREQUENCY/2.0F)
#define DRIVE_JERK_LIMIT_PER_TICK (DRIVE_JERK_LIMIT / CONTROL_LOOP_FREQ)

////////////////////////////////////////////////////////////////////////////////
// Hardware Config
////////////////////////////////////////////////////////////////////////////////

#define PIN_BATTERY_VOLTAGE 28U
#define PIN_DRIVE_FORWARD   16U
#define PIN_DRIVE_REVERSE   17U
#define PIN_DRIVE_VELOCITY  18U
#define PIN_LEDS            0U
#define PIN_STEERING_FRONT  1U
#define PIN_STEERING_REAR   3U

#define LED_FRONT_RIGHT 0U
#define LED_FRONT_LEFT  1U
#define LED_REAR_LEFT   2U
#define LED_REAR_RIGHT  3U

#define DUTY_SERVO1_MIN uint16_t(0xFFFF*0.060F)
#define DUTY_SERVO1_MAX uint16_t(0xFFFF*0.090F)
#define DUTY_SERVO2_MIN uint16_t(0xFFFF*0.060F)
#define DUTY_SERVO2_MAX uint16_t(0xFFFF*0.090F)

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
