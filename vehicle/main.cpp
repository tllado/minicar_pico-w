////////////////////////////////////////////////////////////////////////////////
// MiniZoox Vehicle, Pico-W v0.9.1
// written by Travis Llado
// last edited 2023-11-20
////////////////////////////////////////////////////////////////////////////////

#define DEBUG 1

////////////////////////////////////////////////////////////////////////////////

#if DEBUG
  #include <stdio.h>
#endif

#include "pico/stdlib.h"
#include "hardware/pwm.h"

#include "../common/car_types.h"
#include "../common/llado_math.h"
#include "../common/ws2812.h"

#include "bt_setup.h"
#include "vehicle_config.h"

////////////////////////////////////////////////////////////////////////////////

VehicleCommand command{VehicleCommand{}};
uint32_t message{0U};

////////////////////////////////////////////////////////////////////////////////

void initializeDrive(void) {
  gpio_set_function(PIN_DRIVE_VELOCITY, GPIO_FUNC_PWM);
  uint slice_num = pwm_gpio_to_slice_num(PIN_DRIVE_VELOCITY);
  pwm_config config = pwm_get_default_config();
  pwm_config_set_clkdiv(&config, 4.0);
  pwm_init(slice_num, &config, true);

  gpio_init(PIN_DRIVE_FORWARD);
  gpio_set_dir(PIN_DRIVE_FORWARD, GPIO_OUT);
  gpio_put(PIN_DRIVE_FORWARD, 0);

  gpio_init(PIN_DRIVE_REVERSE);
  gpio_set_dir(PIN_DRIVE_REVERSE, GPIO_OUT);
  gpio_put(PIN_DRIVE_REVERSE, 0);
}

void updateDrive(void) {
  static float motor_command_previous{0.0F};
  float motor_command{0.0F};

  // Filter out tiny velocity commands
  if (abs(command.velocity) > CUTOFF_VELOCITY) {
    motor_command = command.velocity;

    // Limit jerk to improve traction
    if (sign(motor_command) != sign(motor_command_previous)) {
      motor_command_previous = 0.0F;
    }
    if ((abs(motor_command - motor_command_previous) >
            DRIVE_JERK_LIMIT_PER_TICK)) {
      motor_command = motor_command_previous + (DRIVE_JERK_LIMIT_PER_TICK
          * sign(motor_command));
    }
  }

  // Reverse drive direction
  if (command.drive_direction == DriveDirection::that_way) {
    motor_command = -motor_command;
  }

  // Reverse motor controller
  if ((command.velocity >= 0.0F) ^ 
      (command.drive_direction == DriveDirection::that_way)) {
    gpio_put(PIN_DRIVE_FORWARD, 1);
    gpio_put(PIN_DRIVE_REVERSE, 0);
  } else {
    gpio_put(PIN_DRIVE_FORWARD, 0);
    gpio_put(PIN_DRIVE_REVERSE, 1);
  }

  // Write motor command
  const uint32_t range = 0xFFFF;
  const uint32_t duty = (motor_command * motor_command) * range;
  pwm_set_gpio_level(PIN_DRIVE_VELOCITY, duty);
  motor_command_previous =  (command.drive_direction == DriveDirection::this_way) ? motor_command : -motor_command;
}

////////////////////////////////////////////////////////////////////////////////

void initializeSteering(void) {
  gpio_set_function(PIN_STEERING_FRONT, GPIO_FUNC_PWM);
  uint slice_num = pwm_gpio_to_slice_num(PIN_STEERING_FRONT);
  pwm_config config = pwm_get_default_config();
  pwm_config_set_clkdiv(&config, 40.0);
  pwm_init(slice_num, &config, true);
  pwm_set_gpio_level(PIN_STEERING_FRONT, 0U);

  gpio_set_function(PIN_STEERING_REAR, GPIO_FUNC_PWM);
  slice_num = pwm_gpio_to_slice_num(PIN_STEERING_REAR);
  config = pwm_get_default_config();
  pwm_config_set_clkdiv(&config, 40.0);
  pwm_init(slice_num, &config, true);
  pwm_set_gpio_level(PIN_STEERING_REAR, 0U);
}

void updateSteering(void) {
  float front_command{0.0F};
  float rear_command{0.0F};

  if (abs(command.steering) > CUTOFF_STEERING) {
    front_command = command.steering;
  }

  switch (command.steering_mode) {
    case SteeringMode::four_wheel_steer:
      rear_command = -front_command;
      break;
    case SteeringMode::four_wheel_translate:
      rear_command = front_command;
      break;
  }

  if (command.drive_direction == DriveDirection::that_way) {
    std::swap(front_command, rear_command);
  }

  const uint32_t duty_front = ((-front_command + 1.0F) / 2.0F) * 
      (DUTY_SERVO1_MAX - DUTY_SERVO1_MIN) + DUTY_SERVO1_MIN;
  const uint32_t duty_rear = ((-rear_command + 1.0F) / 2.0F) * 
      (DUTY_SERVO2_MAX - DUTY_SERVO2_MIN) + DUTY_SERVO2_MIN;
  pwm_set_gpio_level(PIN_STEERING_FRONT, duty_front);
  pwm_set_gpio_level(PIN_STEERING_REAR, duty_rear);

  #if DEBUG
    printf("servos %f %f %f %u %u\n",
      command.steering,
      front_command,
      rear_command,
      duty_front,
      duty_rear);
  #endif
}

////////////////////////////////////////////////////////////////////////////////

void updateLights() {
  static uint32_t blink_count{0U};

  blink_count++;
  const uint32_t blink_color = 
      ((blink_count / BLINK_TICKS) % 2U) ? COLOR_ORANGE : COLOR_OFF;

  std::vector<uint32_t> leds {
    COLOR_WHITE,
    COLOR_WHITE,
    COLOR_RED,
    COLOR_RED
  };

  if (message == 0U){ // hazards
      leds[LED_FRONT_LEFT] = blink_color;
      leds[LED_FRONT_RIGHT] = blink_color;
      leds[LED_REAR_LEFT] = blink_color;
      leds[LED_REAR_RIGHT] = blink_color;
  } else {
    if (command.velocity > CUTOFF_BRAKING) {
      leds[LED_REAR_RIGHT] = COLOR_OFF;
      leds[LED_REAR_LEFT] = COLOR_OFF;
    } else if (command.velocity < -CUTOFF_BRAKING) {
      const uint32_t color_pink = blend(COLOR_RED, COLOR_WHITE, 0.125F);
      leds[LED_REAR_RIGHT] = color_pink;
      leds[LED_REAR_LEFT] = color_pink;
    }

    if (abs(command.steering) > CUTOFF_TURN_SIGNAL) {        
      if (command.steering > 0.0F) {
        leds[LED_FRONT_RIGHT] = blink_color;
        leds[LED_REAR_RIGHT] = blink_color;
      } else {
        leds[LED_FRONT_LEFT] = blink_color;
        leds[LED_REAR_LEFT] = blink_color;
      }
    }

    if (command.drive_direction == DriveDirection::that_way) {
      std::swap(leds[LED_FRONT_RIGHT], leds[LED_REAR_LEFT]);
      std::swap(leds[LED_FRONT_LEFT], leds[LED_REAR_RIGHT]);
    }
  }

  update_ws2812(leds);
}

////////////////////////////////////////////////////////////////////////////////

bool control_callback(struct repeating_timer *t) {
  if (message == 0U) {
    command.velocity = 0.0F;
    command.steering = 0.0F;
  } else {
    command.velocity = float((message & 0xFFF00000) >> 20U) / 2048.0F - 1.0F;
    command.steering = float((message & 0x000FFF00) >> 8U) / 2048.0F - 1.0F;
    command.drive_direction = 
        static_cast<DriveDirection>((message & 0x00000004) >> 2U);
    command.steering_mode = static_cast<SteeringMode>(message & 0x00000003);
  }

  updateDrive();
  updateSteering();
  updateLights();

  #if DEBUG
    printf("received %f %f %u %u\n", 
      command.velocity,
      command.steering,
      static_cast<uint16_t>(command.drive_direction),
      static_cast<uint16_t>(command.steering_mode));
  #endif

  return true;
 }

////////////////////////////////////////////////////////////////////////////////

int main() {
  #if DEBUG
    stdio_init_all();
  #endif

  repeating_timer_t timer;
  add_repeating_timer_ms((-1000/CONTROL_LOOP_FREQ), control_callback, NULL, &timer);

  initializeDrive();
  initializeSteering();
  init_ws2812(PIN_LEDS);
  bt_receiver_init(&message);

  bt_receiver_start();  // Runs forever, all further activity done by callback

  return 0;
}

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
