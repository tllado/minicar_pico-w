#include <numeric>
#include <vector>
#include <stdint.h>

float abs(const float num) {
  return (num >= 0.0F) ? num : -num;
}

class mean_filter {
public:
  mean_filter(uint32_t length) : length(length) {
    for (uint32_t i = 0U; i < length; i++) {
      data.push_back(0.0F);
    }
  }
  float update(float input) {
    data[index] = input;
    index = (index + 1U) % length;
    return std::accumulate(data.begin(), data.end(), 0.0F) / data.size();
  }
private:
  uint32_t length;
  std::vector<float> data;
  uint32_t index{0U};
};

float sign(const float num) {
  return (num >= 0.0F) ? 1.0F : -1.0F;
}

