enum class DriveDirection {
  this_way = 0U,
  that_way = 1U,
  size = 2U
};

enum class SteeringMode {
  four_wheel_steer = 0U,
  two_wheel_steer = 1U,
  four_wheel_translate = 2U,
  size = 3U
};

struct VehicleCommand {
  float velocity = 0.0F;
  float steering = 0.0F;
  DriveDirection drive_direction = DriveDirection::this_way;
  SteeringMode steering_mode = SteeringMode::four_wheel_steer;
};
